package com.fomin.view;

import com.fomin.controller.ZooController;

import java.util.*;

/**
 * @author Fomin Sergii
 */

public class Menu {
    /**
     * Name of menu.
     */
    private String name;

    /**
     * Description text of menu.
     */
    private String text;

    /**
     * Storage of menu items and methods.
     */
    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    /**
     * Constructor for initializing menu items and controllers.
     *
     * @param zooController - {@link ZooController}
     */
    public Menu(final ZooController zooController) {
        Menu mainMenu = new Menu("Task 04", "main menu");

        mainMenu.putAction("Add animal", zooController::addAnimal);

        mainMenu.putAction("Print all animals", zooController::printAllAnimals);

        mainMenu.putAction("Print all animals by name", zooController::printAllAnimalsByName);

        mainMenu.putAction("Save animals to file", zooController::saveAnimalsToFile);

        mainMenu.putAction("Exit", () -> System.exit(0));

        mainMenu.activateMenu(mainMenu);
    }

    /**
     * Method that starts menu and receive animal console input.
     *
     * @param menu - menu instance to show
     */
    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        int actionNumber;
        while (true) {
            try {
                actionNumber = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Not a number");
                scanner.next();
                continue;
            }
            menu.executeAction(actionNumber);
        }
    }

    /**
     * Method for adding menu item.
     *
     * @param name   - name of item
     * @param action - method that will be called
     */
    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    /**
     * Method that builds menu using {@link StringBuilder}.
     *
     * @return string with all menu items
     */
    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    /**
     * Method for calling action by animal input.
     *
     * @param actionNumber - animal console input
     */
    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
