package com.fomin;

/**
 * @author Fomin Sergii
 */

import com.fomin.controller.ZooController;
import com.fomin.controller.impl.ZooControllerImpl;
import com.fomin.model.dao.ZooDao;
import com.fomin.model.dao.impl.ZooDaoImpl;
import com.fomin.view.Menu;

public class Application {
    public static void main(String[] args) {
        ZooDao zooDao = new ZooDaoImpl();
        ZooController zooController = new ZooControllerImpl(zooDao);

        new Menu(zooController);
    }
}
