package com.fomin.model.dao.impl;

import com.fomin.exceptions.NameNotFoundException;
import com.fomin.exceptions.ListTooLargeException;
import com.fomin.model.dao.ZooDao;
import com.fomin.model.domain.Animal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ZooDaoImpl implements ZooDao {
    private List<Animal> animals;

    public ZooDaoImpl() {
        init();
    }
    private void init() {
        animals = new ArrayList<>();

        List<Animal> data = new ArrayList<Animal>() {
            {
                add(new Animal(
                        "tiger",
                        3,
                        "male"
                ));
                add(new Animal(
                        "elephant",
                        12,
                        "female"
                ));
                add(new Animal(
                        "bear",
                        6,
                        "male"
                ));
                add(new Animal(
                        "giraffe",
                        4,
                        "female"
                ));
                add(new Animal(
                        "penguin",
                        2,
                        "male"
                ));
                add(new Animal(
                        "Fox",
                        3,
                        "female"
                ));
                add(new Animal(
                        "wolf",
                        7,
                        "male"
                ));
                add(new Animal(
                        "marten",
                        2,
                        "male"
                ));
                add(new Animal(
                        "hippopotamus",
                        6,
                        "male"
                ));
                add(new Animal(
                        "jackal",
                        3,
                        "male"
                ));
                add(new Animal(
                        "monkey",
                        4,
                        "female"
                ));
                add(new Animal(
                        "raccoon",
                        2,
                        "female"
                ));
                add(new Animal(
                        "jaguar",
                        5,
                        "male"
                ));
                add(new Animal(
                        "sable",
                        2,
                        "female"
                ));
                add(new Animal(
                        "marten",
                        4,
                        "female"
                ));
                add(new Animal(
                        "wolverine",
                        6,
                        "male"
                ));
                add(new Animal(
                        "deer",
                        1,
                        "male"
                ));
                add(new Animal(
                        "otter",
                        5,
                        "male"
                ));
                add(new Animal(
                        "llama",
                        3,
                        "female"
                ));
            }
        };

        animals.addAll(data);
    }

    private void checkListSize() {
        if (animals.size() >= 20) {
            throw new ListTooLargeException("List can't exceed more than 20 items");
        }
    }

    @Override
    public List<Animal> getAllAnimals() {
        return animals;
    }

    @Override
    public List<Animal> getAllAnimalsByName(String name) throws NameNotFoundException {
        List<Animal> result = animals
                .stream()
                .filter(fn -> fn.getName().equals(name))
                .collect(Collectors.toList());
        if (result.isEmpty()) {
            throw new NameNotFoundException("Name is empty");
        }
        return result;
    }

    @Override
    public void addAnimal(Animal animal) {
        animals.add(animal);
        try {
            checkListSize();
        } catch (ListTooLargeException e) {
            System.out.println("Can't add animal: " + e.getMessage());
        }
    }
}
