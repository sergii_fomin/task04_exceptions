package com.fomin.controller.impl;

import com.fomin.controller.ZooController;
import com.fomin.exceptions.NameNotFoundException;
import com.fomin.model.dao.ZooDao;
import com.fomin.model.domain.Animal;
import com.fomin.service.ZooFileService;
import com.fomin.service.impl.ZooFileServiceImpl;

import java.util.Scanner;

public class ZooControllerImpl implements ZooController {
    private ZooDao animalDao;

    public ZooControllerImpl(ZooDao animalDao) {
        this.animalDao = animalDao;
    }

    @Override
    public void printAllAnimals() {
        animalDao
                .getAllAnimals()
                .forEach(System.out::println);
    }

    @Override
    public void printAllAnimalsByName() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter name - ");
        String name = scanner.next();

        try {
            animalDao.getAllAnimalsByName(name).forEach(System.out::println);
        } catch (NameNotFoundException e) {
            System.out.println("Such name not found");
        }
    }

    @Override
    public void addAnimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter animal data:");

        System.out.print("Name - ");
        String name = scanner.next();

        System.out.print("Age - ");
        int age = scanner.nextInt();

        System.out.print("Sex - ");
        String sex = scanner.next();

        animalDao.addAnimal(new Animal(
                name,
                age,
                sex
        ));
    }

    @Override
    public void saveAnimalsToFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter file name - ");
        String filepath = scanner.next();
        try (ZooFileService animalFileService = new ZooFileServiceImpl()) {
            animalFileService.saveAnimalsToFile(filepath, animalDao.getAllAnimals());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
