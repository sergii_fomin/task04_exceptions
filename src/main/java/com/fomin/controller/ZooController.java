package com.fomin.controller;

public interface ZooController {
    void printAllAnimals();

    void printAllAnimalsByName();

    void addAnimal();

    void saveAnimalsToFile();
}
