package com.fomin.service;

import com.fomin.model.domain.Animal;

import java.util.List;

public interface ZooFileService extends AutoCloseable {
    void saveAnimalsToFile(String filename, List<Animal> animals);
}
